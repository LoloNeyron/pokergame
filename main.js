var max = 51;
var min = 0;

var icoValetR = 'assets/icons/valetR.png' ;
var icoValetB = 'assets/icons/valetN.png';

var icoRoisB = 'assets/icons/roisB.png';
var icoRoisR = 'assets/icons/roisR.png';

var icoReineR = 'assets/icons/queenR.png';
var icoReineB = 'assets/icons/queenB.png';

var icoASB = 'assets/icons/ASB.png';
var icoASR = 'assets/icons/ASR.png';

var icoCarreau = 'assets/icons/carreau.png' 
var icoPic = 'assets/icons/pic.png';
var icoCoeur = 'assets/icons/coeur.png';
var icoTrefle = 'assets/icons/trefle.png';

var deckC = ['1c', '2c', '3c', '4c', '5c', '6c' , '7c', '8c', '9c', 'xc', 'Jc', 'Qc', 'Kc'];
var deckD = ['1d', '2d', '3d', '4d', '5d', '6d' , '7d', '8d', '9d', 'xd', 'Jd', 'Qd', 'Kd'];
var deckP = ['1p', '2p', '3p', '4p', '5p', '6p' , '7p', '8p', '9p', 'xp', 'Jp', 'Qp', 'Kp'];
var deckT = ['1t', '2t', '3t', '4t', '5t', '6t' , '7t', '8t', '9t', 'xt', 'Jt', 'Qt', 'Kt'];

var deckAll = [...deckT, ...deckP, ...deckD, ...deckC];

var nombreDeCartesDivHand = 0;
var nombreDeCartesDivTapis = 0;

var divHand = document.querySelector('#game');
var divTapis = document.querySelector('#tapis');



//Melange le deck et sorts les cartes
function SortDeck() {
    deckAll.sort(function(a, b){return 0.5 - Math.random()});
    
    //TODO: gerer le cas de prendre pas deux fois la meme carte
    var card1 = Math.floor(Math.random() * (max - min) + min);
    var card2 = Math.floor(Math.random() * (max - min) + min);

    var card3 = Math.floor(Math.random() * (max - min) + min);
    var card4 = Math.floor(Math.random() * (max - min) + min);
    var card5 = Math.floor(Math.random() * (max - min) + min);
    var card6 = Math.floor(Math.random() * (max - min) + min);
    var card7 = Math.floor(Math.random() * (max - min) + min);
    
    if(checkIfIsASameCard(card1, card2) == true){
        return SortDeck();
    }else{
        card1 = deckAll[card1];
        card2 = deckAll[card2];

        card3 = deckAll[card3];
        card4 = deckAll[card4];
        card5 = deckAll[card5];
        card6 = deckAll[card6];
        card7 = deckAll[card7];

        var CarteDecompose1 = card1.split('');
        var CarteDecompose2 = card2.split('');

        var CarteDecompose3 = card3.split('');
        var CarteDecompose4 = card4.split('');
        var CarteDecompose5 = card5.split('');
        var CarteDecompose6 = card6.split('');
        var CarteDecompose7 = card7.split('');


        tableCardsN=[CarteDecompose3[0], CarteDecompose4[0], CarteDecompose5[0], CarteDecompose6[0], CarteDecompose7[0]];
        handCardsN=[CarteDecompose1[0], CarteDecompose2[0]];

        document.querySelector('#game').innerHTML = "";
        document.querySelector('#tapis').innerHTML = "";

        showCard(CarteDecompose1, divHand);
        showCard(CarteDecompose2, divHand);

        showCard(CarteDecompose3, divTapis);
        showCard(CarteDecompose4, divTapis);
        showCard(CarteDecompose5, divTapis);
        showCard(CarteDecompose6, divTapis);
        showCard(CarteDecompose7, divTapis);

        checkIfPair(CarteDecompose1, CarteDecompose2, CarteDecompose3, CarteDecompose4, CarteDecompose5, CarteDecompose6, CarteDecompose7);
        checkBrelan(tableCardsN, handCardsN);
    }
}


//affiche Les cartes
function showCard(CarteDecompose, Div, nbr){
    if(CarteDecompose[1] == 'd'){
        creationElement(icoCarreau, 'red', CarteDecompose, Div);
    }else if(CarteDecompose[1] == 'c'){
        creationElement(icoCoeur, 'red', CarteDecompose, Div)
    }else if(CarteDecompose[1] == 't'){
        creationElement(icoTrefle, 'black', CarteDecompose, Div);
    }else if(CarteDecompose[1] == 'p'){
        creationElement(icoPic, 'black', CarteDecompose, Div);
    }
}


function checkIfIsASameCard(card1, card2) {
    if(card1 == card2){
        console.log("SAME CARDS !!");
        return true;
    }else{
        return false;
    }
}

function creationElement(ico, color, CarteDecompose, Div) {
    var divCarte = document.createElement('div');
    divCarte.classList.add('cardStyle');
    var carte1 = document.createElement("img");
    carte1.setAttribute('src',ico);
    var p = document.createElement('span');
    p.innerHTML = (CarteDecompose[0] == "1" ? "A" : (CarteDecompose[0] == "x" ? "10" : CarteDecompose[0]));
    p.setAttribute('style', 'color: '+ color );
    p.setAttribute('style', (CarteDecompose[0] == "x" ? "font-size: 56px" : "font-size: 56px"));
    Div.appendChild(divCarte);
    divCarte.appendChild(p);
    divCarte.appendChild(carte1);
}


function checkIfPair(CarteDecompose1, CarteDecompose2, CarteDecompose3, CarteDecompose4, CarteDecompose5, CarteDecompose6, CarteDecompose7){
    // for(let i = 0 ; i <= allCardsN.lenght; i++){
    //     if(allCardsN[i] = allCardsN[i + 1]){
    //         console.log('pair !')
    //     }
    // }
    if (CarteDecompose1[0] == CarteDecompose2[0]){
        console.log("pair!");
    }else if(CarteDecompose1[0] == CarteDecompose3[0]) {
        console.log("pair!");
    }else if(CarteDecompose1[0] == CarteDecompose4[0]) {
        console.log("pair!");
    }else if(CarteDecompose1[0] == CarteDecompose5[0]) {
        console.log("pair!");
    }else if(CarteDecompose1[0] == CarteDecompose6[0]) {
        console.log("pair!");
    }else if(CarteDecompose1[0] == CarteDecompose7[0]) {
        console.log("pair!");
    }else if(CarteDecompose2[0] == CarteDecompose3[0]) {
        console.log("pair!");
    }else if(CarteDecompose2[0] == CarteDecompose4[0]) {
        console.log("pair!");
    }else if(CarteDecompose2[0] == CarteDecompose5[0]) {
        console.log("pair!");
    }else if(CarteDecompose2[0] == CarteDecompose6[0]) {
        console.log("pair!");
    }else if(CarteDecompose2[0] == CarteDecompose7[0]) {
        console.log("pair!");
    }else if(CarteDecompose3[0] == CarteDecompose4[0]) {
        console.log("pair!");
    }else if(CarteDecompose3[0] == CarteDecompose5[0]) {
        console.log("pair!");
    }else if(CarteDecompose3[0] == CarteDecompose6[0]) {
        console.log("pair!");
    }else if(CarteDecompose3[0] == CarteDecompose7[0]) {
        console.log("pair!");
    }else if(CarteDecompose4[0] == CarteDecompose5[0]) {
        console.log("pair!");
    }else if(CarteDecompose4[0] == CarteDecompose6[0]) {
        console.log("pair!");
    }else if(CarteDecompose4[0] == CarteDecompose7[0]) {
        console.log("pair!");
    }else if(CarteDecompose5[0] == CarteDecompose6[0]) {
        console.log("pair!");
    }else if(CarteDecompose5[0] == CarteDecompose7[0]) {
        console.log("pair!");
    }else if(CarteDecompose6[0] == CarteDecompose7[0]) {
        console.log("pair!");
    }
    
}


function checkBrelan(tableCardsN, handCardsN){

    // var found = tableCardsN.findIndex(function(){
    // return handCardsN[0];})
    function returnNumber(){
        return handCardsN[0];
    }
    console.log('ma fonction detecte les pairs : ' + tableCardsN.every(5) );
}


/*
function checkIfBrelan(CarteDecompose1, CarteDecompose2, CarteDecompose3, CarteDecompose4, CarteDecompose5, CarteDecompose6, CarteDecompose7)
    if(CarteDecompose1 == CarteDecompose2 && CarteDecompose2 == CarteDecompose3){
        console.log('brelan!')
    }
    else if(CarteDecompose1 == CarteDecompose2 && CarteDecompose2 == CarteDecompose4) {

    }


*/